<?php

declare(strict_types=1);

namespace Atg\Core\Helper;

class Store extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Psr\Log\LoggerInterface $logger
    )
    {
        parent::__construct($context);

        $this->storeManager = $storeManager;
        $this->logger = $logger;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
        $storeCode = 'default';
        try {
            $storeCode = $this->storeManager->getStore()->getCode();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $noSuchEntityException) {
            $this->logger->error($noSuchEntityException->getMessage());
        }

        return $storeCode;
    }

}

