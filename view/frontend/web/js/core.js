require(['jquery'], function ($) {
    $(document).ready(function () {
        var navbar = document.getElementById("main-navbar");
        if (navbar != null) {
            var sticky = navbar.offsetTop;

            window.onscroll = function () {
                stickyNavBar()
            };

            function stickyNavBar() {
                var navigationLogo = document.getElementById("navigation-logo");
                if (window.pageYOffset >= sticky) {
                    navigationLogo.style.display = 'block';
                    navbar.classList.add("sticky-navbar");
                } else {
                    navigationLogo.style.display = 'none';
                    navbar.classList.remove("sticky-navbar");
                }
            }
        }
    });
});
